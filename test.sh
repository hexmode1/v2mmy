#!/bin/bash -e

curl=$(command -v curl)
if [ -z "$curl" ]; then
    echo "Curl is not in the path."
    exit 10
fi

jq=$(command -v jq)
if [ -z "$jq" ]; then
    jq=cat
fi

secret="Yes, I really belong here!"
encode=$(command -v base64)
if [ -z "$encode" ]; then
    echo "Base64 is not in the path."
    exit 20
fi
token=$(echo $secret | base64)

auth="Authorization: Bearer $token"
vin=$(curl -s http://randomvin.com/getvin.php?type=fake | tail -1)
echo Looking up info on VIN: $vin
curl -s -H "$auth" http://localhost:10000/$vin | $jq
