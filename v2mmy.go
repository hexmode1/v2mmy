package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"
)

// Some defaults/global settings
var authorized = "Bearer WWVzLCBJIHJlYWxseSBiZWxvbmcgaGVyZSEK"

var (
	vinCheckURL = "https://vpic.nhtsa.dot.gov/api/vehicles/decodevinvalues/"
	usePort     = 10000
	jsonType    = "application/json"
)

type Vehicle struct {
	Make  string `json:"make"`
	Model string `json:"model"`
	Year  int    `json:"year"`
}

type Error struct {
	Msg string `json:"error"`
}

type JSONResponse struct {
	Count   int       `json:"Count"`
	Msg     string    `json:"Message"`
	Results []VinInfo `json:"Results"`
}

type VinInfo struct {
	Error     string `json:"ErrorText"`
	ErrorCode string `json:"ErrorCode"`
	Make      string `json:"Make"`
	Model     string `json:"Model"`
	Year      string `json:"ModelYear"`
}

func main() {
	v2mmyHandler := http.HandlerFunc(v2mmy)
	http.HandleFunc("/", v2mmyHandler)
	log.Println("starting web server on port", usePort)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", usePort), nil))
}

func v2mmy(w http.ResponseWriter, r *http.Request) {
	defer handleError(w, http.StatusUnprocessableEntity)

	if isAuthorized(w, r) && isGET(w, r) {
		returnInfo(w, parseVIN(extractVIN(r)))
	}
}

/*
 * Do we have a valid Auth header header?
 *
 * We'll only allow access to those with Authorization headers that contain a bearer token (see
 * RFC6750 https://datatracker.ietf.org/doc/html/rfc6750#section-2.1 ) that contains the
 * base64-encoded string of 'Yes, I really belong here!'.
 */
func isAuthorized(w http.ResponseWriter, r *http.Request) bool {
	authString := r.Header.Get("authorization")
	if authString != authorized {
		http.Error(w, "Not Authorized", http.StatusUnauthorized)
		return false
	}

	log.Println("Got the correct token: " + authString)
	return true
}

/*
 * Verify that we have a GET method.
 */
func isGET(w http.ResponseWriter, r *http.Request) bool {
	if r.Method != http.MethodGet {
		http.Error(w, "Only GET is allowed", http.StatusMethodNotAllowed)
		return false
	}
	return true
}

/*
 * Extract the VIN from the request.  Here, we're not validating it, we're just grabbing the part of
 * the request that will contain the VIN.
 */
func extractVIN(r *http.Request) string {
	vin := r.URL.Path[1:]
	log.Println("Got VIN: " + vin)

	return vin
}

/*
 * Send the VIN to our vinCheckURL for parsing and return the necessary Vehicle struct.
 */
func parseVIN(vin string) Vehicle {
	if vin == "" {
		panic("You must supply a VIN.")
	}

	reqURL := vinCheckURL + vin + "?format=json"
	log.Println("Requesting data from url: " + reqURL)

	client := http.Client{Timeout: 5 * time.Second}
	resp, err := client.Get(reqURL)
	if err != nil {
		panic(fmt.Sprintf("Got error from VIN parser: %s", err))
	}
	if resp.StatusCode != http.StatusOK {
		panic(fmt.Sprintf("NHTSA's VIN parser did not return OK: %d", resp.StatusCode))
	}
	if resp.Header.Get("content-type") != jsonType {
		panic("NHTSA's VIN parser did not give us JSON!")
	}

	// read body
	var vinResponse JSONResponse
	decoder := json.NewDecoder(resp.Body)

	err = decoder.Decode(&vinResponse)
	if err != nil {
		panic(fmt.Sprintf("Error while decoding VIN response: %s", err))
	}

	return vinInfoToVehicle(vinResponse.Results[0])
}

/*
 * Convert the VinInfo that we get back from NHTSA to a Vehicle struct with only the info we want.
 */
func vinInfoToVehicle(vin VinInfo) Vehicle {
	if vin.ErrorCode != "0" {
		panic(fmt.Sprintf("Error during parsing: %s", vin.Error))
	}

	year, err := strconv.Atoi(vin.Year)
	if err != nil {
		panic(fmt.Sprintf("Error when converting year (%s) to int: %s", vin.Year, err))
	}
	return Vehicle{Make: vin.Make, Model: vin.Model, Year: year}
}

/*
 * Pass the non-error response back to the client.
 */
func returnInfo(w http.ResponseWriter, info Vehicle) {
	j, err := json.Marshal(info)
	if err != nil {
		panic(fmt.Sprintf("Error marshaling Vehicle data into JSON: %s", err))
	}
	w.Header().Set("Content-Type", jsonType)
	w.WriteHeader(http.StatusOK)
	log.Printf("Returning: %s", j)
	n, err := w.Write(j)
	if err != nil {
		// Too late at this point to panic.  May be best to just die
		panic("Couldn't write response to HTTP!")
	}
	log.Printf("Sent %d bytes.", n)
}

/*
 * Ugh.  Something bad happened.  Let the client know.
 */
func handleError(w http.ResponseWriter, code int) {
	if r := recover(); r != nil {
		response := fmt.Sprint(r)
		log.Print(response)
		j, err := json.Marshal(Error{Msg: response})
		if err != nil {
			log.Printf("Error marshaling error: %s", err)
			// We're already in a panic state, manualy construct the error
			http.Error(w, fmt.Sprintf("{ \"Error\": \"Error marshaling error: %s\" }", err),
				http.StatusTeapot)
		} else {
			http.Error(w, string(j), code)
		}
	}
}
