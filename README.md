Given a Vehicle Identification Number (VIN), v2mmy is an API that provides the make, model and year that the VIN represents. This README explains how to run the API as well as how to access it.


# Running the API

To use the API, execute v2mmy in a terminal:

    go run gitlab.com/hexmode1/v2mmy@latest

A webserver will start and a log message will show up on standard output

    2022/05/09 00:41:01 starting web server on port 10000


# Testing the API with test.sh

The included shell script ([test.sh](./test.sh)) will fetch a random, fake VIN from [randomvin.com](http://randomvin.com/) and then pass it to the API with the proper authorization.

The results will be printed out using [jq](https://stedolan.github.io/jq/) (if jq is installed) to format the result.

Following is an example of the test script being run on Linux with `jq` installed:

    $ ./test
    Looking up info on VIN: WMEEJ3BA1DK716122
    {
      "make": "SMART",
      "model": "Fortwo",
      "year": 2013
    }

If `jq` is not installed, the output will be formatted differently.


# Using curl to test the API

The VIN lookup API runs on port 10000 by default.

The API requires you to provide a bearer token (see [RFC 6750](https://datatracker.ietf.org/doc/html/rfc6750#section-2.1)) that contains “Yes, I really belong here!”.  Following the standard of the RFC, the token is base64 encoded.  This results in the following header:

    Authorization: Bearer WWVzLCBJIHJlYWxseSBiZWxvbmcgaGVyZSEK

The VIN you are requesting information on should be the only information in the path of your request.

This brings us to the actual `curl` command that you can use to exercise the API:

    curl -s -H "Authorization: Bearer WWVzLCBJIHJlYWxseSBiZWxvbmcgaGVyZSEK" http://localhost:10000/1J4FY69S7PP211041

If the API is running on localhost, the output would be:

    {"make":"JEEP","model":"Wrangler","year":1993}


# Error handling

The API consumer should only use the result of the API when the HTTP status code returned is 200.

When a non-200 HTTP status code is returned, the API consumer should not use the result for futher processing.  Instead, the API will provide the appropriate messages and HTTP status codes if it is accessed without authorization or if an invalid VIN is supplied.

For example, leaving off the authorization on an otherwise valid request (*e.g.* as in the following `curl` request):

    curl -s http://localhost:10000/1J4FY69S7PP211041

would result in the output:

    Not Authorized

Note that the HTTP status `401 Unauthorized` is also provided. For example, here is the same curl request and the resulting verbose output, including all the headers:

    $ curl -s -v http://localhost:10000/1J4FY69S7PP211041
    *   Trying 127.0.0.1:10000...
    * Connected to localhost (127.0.0.1) port 10000 (#0)
    > GET /1J4FY69S7PP211041 HTTP/1.1
    > Host: localhost:10000
    > User-Agent: curl/7.82.0
    > Accept: */*
    >
    * Mark bundle as not supporting multiuse
    < HTTP/1.1 401 Unauthorized
    < Content-Type: text/plain; charset=utf-8
    < X-Content-Type-Options: nosniff
    < Date: Sun, 08 May 2022 04:37:20 GMT
    < Content-Length: 15
    <
    Not Authorized
    * Connection #0 to host localhost left intact

When an invalid VIN is provided with valid authorization, an HTTP status of `422 Unprocessable Entity` is given.

Here is a verbose, authenticated curl request with the headers in the response included.  In this example, the VIN is invalid because the check digit has been altered with the letter "k".  The altered check digit results in a `422 Unprocessable Entity` HTTP status code and the errors from the NHTSA's service are returned.

    $ curl -v -s -H 'Authorization: Bearer WWVzLCBJIHJlYWxseSBiZWxvbmcgaGVyZSEK' http://localhost:10000/1J4FY69S7PP21104k1 | jq
    *   Trying 127.0.0.1:10000...
    * Connected to localhost (127.0.0.1) port 10000 (#0)
    > GET /1J4FY69S7PP21104k1 HTTP/1.1
    > Host: localhost:10000
    > User-Agent: curl/7.82.0
    > Accept: */*
    > Authorization: Bearer WWVzLCBJIHJlYWxseSBiZWxvbmcgaGVyZSEK
    >
    * Mark bundle as not supporting multiuse
    < HTTP/1.1 422 Unprocessable Entity
    < Content-Type: text/plain; charset=utf-8
    < X-Content-Type-Options: nosniff
    < Date: Sun, 08 May 2022 04:42:21 GMT
    < Content-Length: 127
    <
    { [127 bytes data]
    * Connection #0 to host localhost left intact
    {
      "error": "Error during parsing: 1 - Check Digit (9th position) does not calculate properly; 400 - Invalid Characters Present"
    }


# Customization

Global variables are provided at the top of [v2mmy.go](./v2mmy.go) that may be changed, if desired.

    // Some defaults/global settings
    var authorized = "Bearer WWVzLCBJIHJlYWxseSBiZWxvbmcgaGVyZSEK";
    var vinCheckURL = "https://vpic.nhtsa.dot.gov/api/vehicles/decodevinvalues/";
    var usePort = 10000;
    var jsonType = "application/json";

If you want to change the authorization token the API client needs to provide in the `Authorization` header, you can do so by changing the value of the `authorized` variable.

If you have an alternate API *with an identical interface* to the one provided by NHTSA, you can switch to it by changing `vinCheckURL`.

If port 10000 is already used and v2mmy fails to execute as a result, you can change `usePort` so that another port is used. Remember to use the new port when accessing the API.

Regarding `jsonType`: this should **not** be changed since the type is a standard MIME type.

